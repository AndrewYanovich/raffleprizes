<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180114145404 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE prize_type (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_3CA7C8E5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE product (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, qty INT NOT NULL, UNIQUE INDEX UNIQ_D34A04AD5E237E06 (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE raffled_prize (id INT AUTO_INCREMENT NOT NULL, user_id INT NOT NULL, qty INT NOT NULL, action_type VARCHAR(255) NOT NULL, status VARCHAR(255) NOT NULL, prize_type INT NOT NULL, INDEX IDX_3524F501A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE raffled_prize ADD CONSTRAINT FK_3524F501A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');

        $this->addSql('INSERT INTO product (name, qty) VALUES (\'Product1\', \'10\')');
        $this->addSql('INSERT INTO product (name, qty) VALUES (\'Product2\', \'20\')');
        $this->addSql('INSERT INTO product (name, qty) VALUES (\'Product3\', \'30\')');

        $this->addSql('INSERT INTO prize_type (name) VALUES (\'Money\')');
        $this->addSql('INSERT INTO prize_type (name) VALUES (\'Loyalty Points\')');
        $this->addSql('INSERT INTO prize_type (name) VALUES (\'Product\')');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE prize_type');
        $this->addSql('DROP TABLE product');
        $this->addSql('DROP TABLE raffled_prize');
    }
}
