<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180115090746 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE raffled_prize CHANGE prize_type prize_type_id INT NOT NULL');
        $this->addSql('ALTER TABLE raffled_prize ADD CONSTRAINT FK_3524F50175D1EEED FOREIGN KEY (prize_type_id) REFERENCES prize_type (id)');
        $this->addSql('CREATE INDEX IDX_3524F50175D1EEED ON raffled_prize (prize_type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE raffled_prize DROP FOREIGN KEY FK_3524F50175D1EEED');
        $this->addSql('DROP INDEX IDX_3524F50175D1EEED ON raffled_prize');
        $this->addSql('ALTER TABLE raffled_prize CHANGE prize_type_id prize_type INT NOT NULL');
    }
}
