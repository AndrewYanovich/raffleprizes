<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 14.01.18
 * Time: 15:32
 */

namespace AppBundle\Controller;


use AppBundle\Entity\Config;
use AppBundle\Entity\PrizeType;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductToShip;
use AppBundle\Entity\RaffledPrize;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class MainController extends Controller
{
    public function homepageAction()
    {
        $em = $this->getDoctrine()->getManager();

        $user = $this->get('security.token_storage')->getToken()->getUser();
        $prizes = [];
        if ($user instanceof User){
            $prizes = $em->getRepository(RaffledPrize::class)->getRaffledPrizesForUser($user);
        }
//dump($prizes);die;
        return $this->render('main/homepage.html.twig',[
            'prizes' => $prizes,
            'user' => $user
        ]);
    }

    /**
     * @Route("/raffle", name="raffle")
     */
    public function getPrize(){

        $em = $this->getDoctrine()->getManager();
        $prizesTypes = $em->getRepository(PrizeType::class)->findAll();

        if (count($prizesTypes) < 1) {
            $this->addFlash('error', 'Nothing to raffle. Sorry :(');
        } else {
            $isRaffled = false;
            while (!$isRaffled) {
                $prize = $prizesTypes[rand(0, count($prizesTypes) - 1)];
                switch ($prize->getName()) {
                    case 'Money':
                        $isRaffled = $this->raffleMoney($prize);
                        break;

                    case 'Loyalty Points':
                        $isRaffled = $this->raffleLoyaltyPoints($prize);
                        break;

                    case 'Product':
                        $isRaffled = $this->raffleProduct($prize);
                        break;
                }
            }
        }

        return $this->redirectToRoute('homepage');
    }

    private function raffleMoney($prize){
        $em = $this->getDoctrine()->getManager();
        $moneyTotals = $em->getRepository(Config::class)->findOneBy(['name' => 'money_totals']);
        if($moneyTotals->getValue() > 0){
            $raffledMoney = rand(1, 10);
            $moneyTotals->setValue($moneyTotals->getValue() - $raffledMoney);
            $em->persist($moneyTotals);
            $em->flush();

            $this->addPrizeToDB($prize, $raffledMoney);

            $this->addFlash('success', 'You raffled '.$raffledMoney.' Money');
            return true;
        } else {
            return false;
        }
    }

    private function raffleLoyaltyPoints($prize){
            $raffledLoyaltyPoints = rand(1, 10);

            $em = $this->getDoctrine()->getManager();
            /** @var User $user */
            $user = $em->getRepository(User::class)->findOneBy(['id' => $this->get('security.token_storage')->getToken()->getUser()->getId()]);
            $user->setLoyaltyPoints($raffledLoyaltyPoints);
            $em->persist($user);
            $em->flush();

            $this->addPrizeToDB($prize, $raffledLoyaltyPoints);
            $this->addFlash('success', 'You raffled '.$raffledLoyaltyPoints.' Loyalty Points');
            return true;
    }

    private function raffleProduct($prize){
        $em = $this->getDoctrine()->getManager();
        $products = $em->getRepository(Product::class)->findAll();
        if (count($products) < 1){
            return false;
        } else {
            $product = $products[rand(0, count($products) - 1)];
            if($product->getQty() < 1){
                return false;
            } else {

                $raffledPrize = new RaffledPrize();
                $raffledPrize->setPrizeType($prize);
                $raffledPrize->setQty(1);
                $raffledPrize->setActionType($product->getName());
                $raffledPrize->setStatus('pending');
                $raffledPrize->setUser($this->get('security.token_storage')->getToken()->getUser());

                $em = $this->getDoctrine()->getManager();
                $em->persist($raffledPrize);
                $em->flush();

                $this->addFlash('success', 'You raffled 1 '.$product->getName());
                return true;
            }
        }
    }

    private function addPrizeToDB($prize, $qty){

        $raffledPrize = new RaffledPrize();
        $raffledPrize->setPrizeType($prize);
        $raffledPrize->setQty($qty);
        if($prize->getName() == 'Loyalty Points'){
            $raffledPrize->setActionType('enrolled');
            $raffledPrize->setStatus('success');
        } else {
            $raffledPrize->setActionType('undefined');
            $raffledPrize->setStatus('pending');
        }
        $raffledPrize->setUser($this->get('security.token_storage')->getToken()->getUser());

        $em = $this->getDoctrine()->getManager();
        $em->persist($raffledPrize);
        $em->flush();
//        $moneyTotals = $em->getRepository(RaffledPrize::class)->findOneBy(['name' => 'money_totals']);
    }
}