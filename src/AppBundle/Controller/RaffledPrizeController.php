<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 15.01.18
 * Time: 13:16
 */

namespace AppBundle\Controller;

use AppBundle\Entity\Config;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductToShip;
use AppBundle\Entity\RaffledPrize;
use AppBundle\Entity\User;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class RaffledPrizeController extends Controller
{
    /**
     * @Route("/convert_money/{raffledPrizeId}", name="convert_money")
     * @param $raffledPrizeId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function convertMoney($raffledPrizeId)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var Config $config */
        $config = $em->getRepository(Config::class)->findOneBy(['name' => 'money_to_points_ratio']);

        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneBy(['id' => $this->get('security.token_storage')->getToken()->getUser()->getId()]);

        /** @var RaffledPrize $raffledPrize */
        $raffledPrize = $em->getRepository(RaffledPrize::class)->findOneBy(['id' => $raffledPrizeId]);

        $user->setLoyaltyPoints($user->getLoyaltyPoints() + $raffledPrize->getQty() * $config->getValue());

        $raffledPrize->setStatus('success');
        $raffledPrize->setActionType('converted '.$raffledPrize->getPrizeType());
        $em->persist($raffledPrize);
        $em->flush();

        $this->addFlash('success', 'You converted '.$raffledPrize->getQty().' money to '.$raffledPrize->getQty() * $config->getValue().' Loyalty Points');

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/enroll_money/{raffledPrizeId}", name="enroll_money")
     * @param $raffledPrizeId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function enrollMoney($raffledPrizeId)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneBy(['id' => $this->get('security.token_storage')->getToken()->getUser()->getId()]);
        // todo the request to bank with money amount and user credentials.

        /** @var RaffledPrize $raffledPrize */
        $raffledPrize = $em->getRepository(RaffledPrize::class)->findOneBy(['id' => $raffledPrizeId]);
        $raffledPrize->setStatus('success');
        $raffledPrize->setActionType('enrolled '.$raffledPrize->getPrizeType());
        $em->persist($raffledPrize);
        $em->flush();

        $this->addFlash('success', 'You enrolled '.$raffledPrize->getQty().' money');

        return $this->redirectToRoute('homepage');
    }

    /**
     * @Route("/accept_prize/{raffledPrizeId}", name="accept_prize")
     * @param $raffledPrizeId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function acceptPrize($raffledPrizeId)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var RaffledPrize $raffledPrize */
        $raffledPrize = $em->getRepository(RaffledPrize::class)->findOneBy(['id' => $raffledPrizeId]);

        /** @var Product $product */
        $productName = $raffledPrize->getActionType();
        $product = $em->getRepository(Product::class)->findOneBy(['name' => $productName]);
        $product->setQty($product->getQty() - 1);
        $em->persist($product);
        $em->flush();

        $raffledPrize->setStatus('success');
        $raffledPrize->setActionType('accepted '.$productName);
        $em->persist($raffledPrize);
        $em->flush();

        $productToShip = new ProductToShip();
        $productToShip->setQty(1);
        $productToShip->setTitle('Need to ship '.$product->getName().' to user '.$this->get('security.token_storage')->getToken()->getUser()->getUsername());
        $em->persist($productToShip);
        $em->flush();

        $this->addFlash('success', 'You accepted the '.$productName);

        return $this->redirectToRoute('homepage');

    }

    /**
     * @Route("/decline_prize/{raffledPrizeId}", name="decline_prize")
     * @param $raffledPrizeId
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function declinePrize($raffledPrizeId)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var RaffledPrize $raffledPrize */
        $raffledPrize = $em->getRepository(RaffledPrize::class)->findOneBy(['id' => $raffledPrizeId]);
        $raffledPrize->setStatus('success');
        $raffledPrize->setActionType('declined '.$raffledPrize->getPrizeType());
        $em->persist($raffledPrize);
        $em->flush();

        $this->addFlash('error', 'You declined '.$raffledPrize->getQty().' '.$raffledPrize->getPrizeType());

        return $this->redirectToRoute('homepage');
    }
}