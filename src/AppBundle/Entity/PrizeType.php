<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 14.01.18
 * Time: 17:05
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PrizeTypeRepository")
 * @ORM\Table(name="prize_type")
 */
class PrizeType
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", unique=true)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="RaffledPrize", mappedBy="prizeType")
     */
    private $raffledPrizes;
    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return mixed
     */
    public function getRaffledPrizes()
    {
        return $this->raffledPrizes;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @param mixed $raffledPrizes
     */
    public function setRaffledPrizes($raffledPrizes)
    {
        $this->raffledPrizes = $raffledPrizes;
    }

    public function __toString()
    {
        return $this->name;
    }

}