<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 14.01.18
 * Time: 17:23
 */

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class PrizeTypeRepository extends  EntityRepository
{
    public function countPrizeTypes(){
        return $this->createQueryBuilder('pt')
            ->select('count(pt.id)')
            ->from('AppBundle::PrizeType', 'pt')
            ->getQuery()
            ->getSingleScalarResult();
    }
}