<?php
/**
 * Created by PhpStorm.
 * User: andrew
 * Date: 14.01.18
 * Time: 20:11
 */

namespace AppBundle\Repository;

use AppBundle\Entity\PrizeType;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;

class RaffledPrizeRepository extends  EntityRepository
{
    public function getRaffledPrizesForUser(User $user){
//        return $this->createQueryBuilder('rp')
//            ->andWhere('rp.user = :user')
//            ->setParameter('user', $user)
//            ->leftJoin('rp.prizeType', 'prize_type')
//            ->addOrderBy('rp.id', 'DESC')
//            ->getQuery()
//            ->execute();

        $query = $this->getEntityManager()
            ->createQuery('SELECT rp.id, rp.qty, rp.actionType, rp.status, pt.name
                            FROM AppBundle:RaffledPrize rp
                            INNER JOIN rp.prizeType pt
                            WHERE rp.user = :user ORDER BY rp.id DESC')
            ->setParameter('user', $user);
//dump($query->getSQL());die;
//        SELECT *, pt.name AS pt_name FROM raffled_prize rp INNER JOIN prize_type pt ON rp.prize_type = pt.id WHERE rp.user_id = 2;
        return $query->getResult();

//        $repository = $this->getDoctrine()
//            ->getRepository('AppBundle:Product');
//
//        $query = $repository->createQueryBuilder('p')
//            ->where('p.price > :price')
//            ->setParameter('price', '19.99')
//            ->orderBy('p.price', 'ASC')
//            ->getQuery();
//
//        $products = $query->getResult();
//
//
//        $query = $this->getEntityManager()
//            ->createQuery(
//                'SELECT rp, pt FROM AppBundle:RaffledPrize rp
//            JOIN rp.prize_type pt
//            WHERE rp.id = :id'
//            )->setParameter('id', $id);
    }
}